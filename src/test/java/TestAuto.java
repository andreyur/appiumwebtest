
import Pages.*;
import Utils.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import java.net.URISyntaxException;
import static Pages.BasePage.BASE_URL;



public class TestAuto {
    WebDriver driver;
    HomePage homePage;

    @BeforeTest
    public void beforeTest(){
        // Initialization WebDriver
        driver= DriverFactory.getDriver();
    }

    @BeforeMethod
    public void beforeMethod(){
        homePage = new HomePage(driver);
        homePage.open(BASE_URL);
    }

    @Test (priority = 1)
    public void testHomePage() throws URISyntaxException {
        homePage.shouldHaveHost(HomePage.HOST);
        homePage.shouldHaveTitle(HomePage.TITLE);
        homePage.shouldHaveSearchForm();
        homePage.shouldHaveLogo();
    }

    @Test (dataProvider = "Search")
    public void testSearch(String category, String brand, String model, String year){
        SearchResultsPage searchResultsPage = homePage.search(category, brand, model, year);
        searchResultsPage.shouldHaveSearchResults(10);
        searchResultsPage.shouldHaveBrandAndModelInSearchResults(brand, model);
    }
    @Test
    public void testProductPage(){
        String category = "Легковые";
        String brand = "Volkswagen";
        String model = "Jetta";
        String year = "2015";
        SearchResultsPage searchResultsPage = homePage.search(category, brand, model, year);
        ProductPage productPage = searchResultsPage.clickRandomSearchResult();
        productPage.shouldHaveLogo();
        productPage.shouldHaveBrandAndModelInHeadingH3(brand, model);
    }
    @Test
    public void testShareButtonsOnFooter(){
        Footer footer = new Footer(driver);
        footer.scrollToLinks();
        footer.shouldHaveInstagramLink();
        footer.shouldHaveTwitterLink();
        footer.shouldHaveFacebookLink();
        footer.shouldHaveTelegramLink();
    }
    @DataProvider(name = "Search")
    public static Object[][] search() {
        return new Object[][]{
                {"Легковые", "Volkswagen", "Jetta", "2015"},
                {"Легковые", "BMW", "X5 M", "2016"},
                {"Легковые","Ford", "Fusion", "2014"}
        };
    }

    @AfterTest
    public void afterTest(){
        //Close the browser
        driver.quit();;
    }
}
