package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import static org.hamcrest.MatcherAssert.assertThat;

public class Footer extends BasePage {
    public Footer(WebDriver driver){
        super(driver);
    }
    private static final String INSTAGRAM_LINK = "a[href*='instagram']";
    private static final String TWITTER_LINK = "a[href*='twitter']";
    private static final String FACEBOOK_LINK = "a[href*='facebook']";
    private static final String TELEGRAM_LINK = "a[href*='t.me']";

    @FindBy(css=INSTAGRAM_LINK)
    private WebElement instagramLink;
    public WebElement getInstagramLink() {
        return instagramLink;
    }

    @FindBy(css=TWITTER_LINK)
    private WebElement twitterLink;
    public WebElement getTwitterLink() {
        return twitterLink;
    }

    @FindBy(css=FACEBOOK_LINK)
    private WebElement facebookLink;
    public WebElement getFacebookLink() {
        return facebookLink;
    }

    @FindBy(css=TELEGRAM_LINK)
    private WebElement telegramLink;
    public WebElement getTelegramLink() {
        return telegramLink;
    }

    public void scrollToLinks(){
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", getInstagramLink());
    }

    public void shouldHaveInstagramLink(){
        assertThat("Instagram link should be displayed",getInstagramLink().isDisplayed());
    }

    public void shouldHaveTwitterLink(){
        assertThat("Twitter link should be displayed",getTwitterLink().isDisplayed());
    }

    public void shouldHaveFacebookLink(){
        assertThat("Facebook link should be displayed",getFacebookLink().isDisplayed());
    }

    public void shouldHaveTelegramLink(){
        assertThat("Telegram link should be displayed",getTelegramLink().isDisplayed());
    }
}
