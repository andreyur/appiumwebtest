package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.net.URI;
import java.net.URISyntaxException;

public class HomePage extends BasePage  {
    public HomePage(WebDriver driver){
        super(driver);
    }
    public static final String HOST = "auto.ria.com";
    public static final String TITLE = "AUTO.RIA™ — Автобазар №1. Купить и продать авто легко как никогда";
    public static final String SEARCH_FORM = "div.form-search";
    public static final String LOGO = "a.logo";
    public static final String LOGIN_LINK = "a.item[href*='login.html']";
    public static final String RADIO_BUTTON_TYPE = "[for='buRadioType']";
    public static final String SELECT_CATEGORIES = "#categories";
    public static final String BRAND_INPUT = "#brandTooltipBrandAutocompleteInput-brand";
    public static final String MODEL_INPUT = "#brandTooltipBrandAutocompleteInput-model";
    public static final String SELECT_YEAR = "#yearFrom";
    public static final String SEARCH_BUTTON = "[type='submit']";


    @FindBy (css = SEARCH_FORM)
    private WebElement searchForm;
    public WebElement getSearchForm() {
        return searchForm;
    }

    @FindBy (css = LOGO)
    private WebElement logo;
    public WebElement getLogo() {
        return logo;
    }

    @FindBy (css = LOGIN_LINK)
    private WebElement loginLink;
    public WebElement getLoginLink() {
        return loginLink;
    }

    @FindBy (css = RADIO_BUTTON_TYPE)
    private WebElement radioButtonType;
    public WebElement getRadioButtonType() {
        return radioButtonType;
    }

    @FindBy (css = SELECT_CATEGORIES)
    private WebElement selectCategories;
    public WebElement getSelectCategories() {
        return selectCategories;
    }

    @FindBy (css = BRAND_INPUT)
    private WebElement brandInput;
    public WebElement getBrandInput() {
        return brandInput;
    }

    @FindBy (css = MODEL_INPUT)
    private WebElement modelInput;
    public WebElement getModelInput() {
        return modelInput;
    }

    @FindBy (css = SELECT_YEAR)
    private WebElement selectYear;
    public WebElement getSelectYear() {
        return selectYear;
    }

    @FindBy (css = SEARCH_BUTTON)
    private WebElement searchButton;
    public WebElement getSearchButton() {
        return searchButton;
    }

    public HomePage open(String url){
        driver.get(url);
        return this;
    }

    public SearchResultsPage search(String category, String brand, String model, String year){
        assertThat("Radio button should be displayed",getRadioButtonType().isDisplayed());
        getRadioButtonType().click();
        Select categorySelect = new Select(getSelectCategories());
        categorySelect.selectByVisibleText(category);
        assertThat(categorySelect.getFirstSelectedOption().getText(), equalTo(category));
        assertThat("Input for Brand should be displayed",getBrandInput().isDisplayed());
        getBrandInput().sendKeys(brand);
        waitForTextToAppear(By.linkText(brand), brand);
        assertThat("Brand link should be displayed",driver.findElement(By.linkText(brand)).isDisplayed());
        driver.findElement(By.linkText(brand)).click();
        assertThat("Input for Model should be displayed",getModelInput().isDisplayed());
        getModelInput().sendKeys(model);
        waitForTextToAppear(By.linkText(model), model);
        assertThat("Model link should be displayed",driver.findElement(By.linkText(model)).isDisplayed());
        driver.findElement(By.linkText(model)).click();
        Select yearSelect = new Select(getSelectYear());
        yearSelect.selectByVisibleText(year);
        assertThat(yearSelect.getFirstSelectedOption().getText(), equalTo(year));
        assertThat("Search Button should be displayed",getSearchButton().isDisplayed());
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", getSearchButton());
        getSearchButton().click();
        return new SearchResultsPage(driver);
    }

    public void shouldHaveHost(String host) throws URISyntaxException {
        assertThat(new URI(driver.getCurrentUrl()).getHost(), equalTo("auto.ria.com"));
    }
    public void shouldHaveTitle(String title){
        assertThat(driver.getTitle(), equalTo(title));
    }

    public void shouldHaveSearchForm(){
        assertThat("Search Form should be displayed",searchForm.isDisplayed());
    }

    public void shouldHaveLogo(){
        assertThat("Logo should be displayed",logo.isDisplayed());
    }
}
