package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.Random;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

public class SearchResultsPage extends BasePage {
    public SearchResultsPage(WebDriver driver){
        super(driver);
    }

    public static final String SECTION = "section.ticket-item";
    public static final String SEARCH_RESULT_PHOTO = "a.m-link-ticket";

    @FindBy(css = SECTION)
    private List<WebElement> listSearch;
    public List<WebElement> getListSearch() {
        return listSearch;
    }

    @FindBy(css=SEARCH_RESULT_PHOTO)
    private List<WebElement> searchResultPhoto;
    public List<WebElement> getSearchResultPhoto() {
        return searchResultPhoto;
    }

    public void shouldHaveSearchResults(int searchResults){
        assertThat(getListSearch(), hasSize(10));
    }

    public void shouldHaveBrandAndModelInSearchResults(String brand, String model){
        List<WebElement> list = getListSearch();
        for (WebElement element : list) {
            assertThat(element.getText(), is(containsString(brand+" "+model)));
        }
    }

    public ProductPage clickRandomSearchResult(){
        WebElement randomElement = getSearchResultPhoto().get(new Random().nextInt(getListSearch().size()));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", randomElement);
        randomElement.click();
        return new ProductPage(driver);
    }
}
