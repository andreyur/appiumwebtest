package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ProductPage extends BasePage {
    public ProductPage(WebDriver driver){
        super(driver);
    }
    public static final String LOGO = "header.app-header-mobile>a[href*='auto.ria']";
    private static final String HEADING_H3 = "h3.auto-content_title";

    @FindBy (css = LOGO)
    private WebElement logo;
    public WebElement getLogo() {
        return logo;
    }

    @FindBy(css=HEADING_H3)
    private WebElement headingH3;
    public WebElement getHeadingH3() {
        return headingH3;
    }

    public void shouldHaveLogo(){
        assertThat("Logo should be displayed",logo.isDisplayed());
    }

    public void shouldHaveBrandAndModelInHeadingH3(String brand, String model){
        assertThat(getHeadingH3().getText(), is(containsString(brand+" "+model)));
    }
}
