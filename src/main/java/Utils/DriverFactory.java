package Utils;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import org.openqa.selenium.remote.DesiredCapabilities;
import static io.appium.java_client.remote.MobileCapabilityType.*;
import static io.appium.java_client.remote.MobileCapabilityType.NEW_COMMAND_TIMEOUT;
import static io.appium.java_client.remote.MobileCapabilityType.NO_RESET;
import static io.appium.java_client.service.local.AppiumDriverLocalService.buildService;
import static org.openqa.selenium.remote.CapabilityType.PLATFORM_NAME;

public class DriverFactory {
    public static AndroidDriver<MobileElement> getDriver (){
        String deviceName = "053c9fda0025ee8b";
        AppiumDriverLocalService service;
        service = buildService(new AppiumServiceBuilder().usingAnyFreePort());
        service.start();
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(DEVICE_NAME, deviceName);
        capabilities.setCapability(PLATFORM_NAME, "Android");
        capabilities.setCapability(BROWSER_NAME, "Chrome");
        capabilities.setCapability(FULL_RESET, false);
        capabilities.setCapability(CLEAR_SYSTEM_FILES, false);
        capabilities.setCapability(NO_RESET, true);
        capabilities.setCapability(NEW_COMMAND_TIMEOUT, 600);
        return new AndroidDriver<MobileElement>(service.getUrl(), capabilities);
    }
}
